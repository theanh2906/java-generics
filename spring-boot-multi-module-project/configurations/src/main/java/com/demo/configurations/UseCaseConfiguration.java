package com.demo.configurations;

import com.demo.usercases.FindAllAccountsUseCase;
import com.demo.usercases.adapters.AccountAdapter;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class UseCaseConfiguration {
    @Bean
    public FindAllAccountsUseCase findAllAccountsUseCase(AccountAdapter accountAdapter) {
        return new FindAllAccountsUseCase(accountAdapter);
    }
}
