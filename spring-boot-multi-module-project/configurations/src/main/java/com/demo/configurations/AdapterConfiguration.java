package com.demo.configurations;

import com.demo.databases.adapters.AccountAdapterImpl;
import com.demo.usercases.adapters.AccountAdapter;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class AdapterConfiguration {
    @Bean
    public AccountAdapter accountAdapter() {
        return new AccountAdapterImpl();
    }
}
