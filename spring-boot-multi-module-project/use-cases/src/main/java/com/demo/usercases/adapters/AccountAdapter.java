package com.demo.usercases.adapters;

import com.demo.entities.Account;
import reactor.core.publisher.Flux;

public interface AccountAdapter {
    Flux<Account> findAll();
}
