package com.demo.usercases;

import com.demo.entities.Account;
import com.demo.usercases.adapters.AccountAdapter;
import reactor.core.publisher.Flux;

public class FindAllAccountsUseCase {
    private AccountAdapter accountAdapter;

    public FindAllAccountsUseCase(AccountAdapter accountAdapter) {
        this.accountAdapter = accountAdapter;
    }

    public Flux<Account> execute() {
        return accountAdapter.findAll();
    }
}
