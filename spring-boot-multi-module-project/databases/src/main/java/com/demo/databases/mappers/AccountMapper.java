package com.demo.databases.mappers;

import com.demo.databases.models.AccountModel;
import com.demo.entities.Account;
import org.springframework.beans.BeanUtils;

public class AccountMapper {
    public static AccountModel toModel(Account entity) {
        final AccountModel model = AccountModel.getInstance();
        BeanUtils.copyProperties(entity, model);
        return model;
    }
    public static Account toEntity(AccountModel model) {
        final Account entity = Account.getInstance();
        BeanUtils.copyProperties(model, entity);
        return entity;
    }
}
