package com.demo.databases.adapters;

import com.demo.databases.AccountRepository;
import com.demo.databases.mappers.AccountMapper;
import com.demo.databases.models.AccountModel;
import com.demo.entities.Account;
import com.demo.usercases.adapters.AccountAdapter;
import org.springframework.beans.factory.annotation.Autowired;
import reactor.core.publisher.Flux;

import java.util.Comparator;

public class AccountAdapterImpl implements AccountAdapter {
    @Autowired
    private AccountRepository accountRepository;
    @Override
    public Flux<Account> findAll() {
        return accountRepository
                .findAll()
                .sort(Comparator.comparing(AccountModel::getFullname))
                .map(AccountMapper::toEntity);
    }
}
