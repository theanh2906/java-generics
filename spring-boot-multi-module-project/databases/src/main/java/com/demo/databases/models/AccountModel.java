package com.demo.databases.models;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.springframework.data.annotation.Id;
import org.springframework.data.relational.core.mapping.Column;
import org.springframework.data.relational.core.mapping.Table;

@Getter
@Setter
@NoArgsConstructor
@Table("accounts")
public class AccountModel {

    @Id
    private String id;

    @Column
    private String fullname;

    @Column
    private String username;

    @Column
    private String password;

    @Column
    private String email;

    public static AccountModel getInstance() {
        return new AccountModel();
    }
}
