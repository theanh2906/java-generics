package com.demo.rests.dtos;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@NoArgsConstructor
public class AccountDto {
    private String username;
    private String fullname;

    public static AccountDto getInstance() {
        return new AccountDto();
    }
}
