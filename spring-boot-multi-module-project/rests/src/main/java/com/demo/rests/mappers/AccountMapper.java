package com.demo.rests.mappers;

import com.demo.entities.Account;
import com.demo.rests.dtos.AccountDto;
import org.springframework.beans.BeanUtils;

public class AccountMapper {
    public static Account toEntity(AccountDto dto) {
        final Account entity = Account.getInstance();
        BeanUtils.copyProperties(entity, dto);
        return entity;
    }
    public static AccountDto toDto(Account entity) {
        final AccountDto dto = AccountDto.getInstance();
        BeanUtils.copyProperties(entity, dto);
        return dto;
    }
}
