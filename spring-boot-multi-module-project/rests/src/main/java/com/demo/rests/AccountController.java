package com.demo.rests;

import com.demo.rests.dtos.AccountDto;
import com.demo.rests.mappers.AccountMapper;
import com.demo.usercases.FindAllAccountsUseCase;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;
import reactor.core.publisher.Flux;

@RestController
public class AccountController {
    @Autowired
    private FindAllAccountsUseCase findAllAccountsUseCase;

    @GetMapping("/accounts")
    public Flux<AccountDto> findAllAccounts() {
        return findAllAccountsUseCase.execute().map(AccountMapper::toDto);
    }
}
