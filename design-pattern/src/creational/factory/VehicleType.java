package creational.factory;

public enum VehicleType {
    CAR, SCOOTER
}
