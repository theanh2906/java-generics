package creational.factory;

public class Implement {
    public static void main(String[] args) {
        Vehicle car = VehicleFactory.getVehicle(VehicleType.CAR);
        assert car != null;
        System.out.println(car.getVehicleName());
        Vehicle scooter = VehicleFactory.getVehicle(VehicleType.SCOOTER);
        assert scooter != null;
        System.out.println(scooter.getVehicleName());
    }
}
