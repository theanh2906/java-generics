package creational.factory;

public class Scooter implements Vehicle{
    @Override
    public String getVehicleName() {
        return "Piaggio Liberty";
    }
}
