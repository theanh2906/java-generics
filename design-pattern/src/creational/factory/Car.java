package creational.factory;

public class Car implements Vehicle{
    @Override
    public String getVehicleName() {
        return "Toyota Fortuner";
    }
}
