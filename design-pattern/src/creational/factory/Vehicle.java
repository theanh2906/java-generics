package creational.factory;

public interface Vehicle {
    String getVehicleName();
}
