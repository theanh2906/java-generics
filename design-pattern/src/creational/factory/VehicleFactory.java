package creational.factory;

public class VehicleFactory {
    VehicleFactory() {}
    public static Vehicle getVehicle(VehicleType vehicleType) {
        if (vehicleType == VehicleType.CAR) {
            return new Car();
        } else if (vehicleType == VehicleType.SCOOTER) {
            return new Scooter();
        } else {
            return null;
        }
    }
}
