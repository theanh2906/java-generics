package creational.singleton;

public class Car {
    private String status;
    private Size size;
    private Color color;

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public Size getSize() {
        return size;
    }

    public void setSize(Size size) {
        this.size = size;
    }

    public Color getColor() {
        return color;
    }

    public void setColor(Color color) {
        this.color = color;
    }

    public static Car getInstance() {
        Car car = new Car();
        car.setColor(new Color("white"));
        car.setSize(new Size("big"));
        return car;
    }

    @Override
    public String toString() {
        return "{ color: '" + this.color.getColor() + "', size: '" + this.size.getSize() + "'}";
    }
}
