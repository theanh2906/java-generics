package creational.builder;

import creational.builder.type.ColorType;
import creational.builder.type.SizeType;

public interface CarBuilder {
    CarBuilder size(SizeType sizeType);
    CarBuilder color(ColorType color);
    Car build();
    static CarBuilderImpl newBuilder() {
        return new CarBuilderImpl();
    }
}
