package creational.builder;

import creational.builder.type.ColorType;
import creational.builder.type.SizeType;

public class Car {
    private SizeType sizeType;
    private ColorType colorType;

    Car(SizeType sizeType, ColorType colorType) {
        this.sizeType = sizeType;
        this.colorType = colorType;
    }

    public SizeType getSizeType() {
        return sizeType;
    }

    public ColorType getColorType() {
        return colorType;
    }

    @Override
    public String toString() {
        return "Car [sizeType=" + this.sizeType + ", colorType=" + this.colorType + "]";
    }
}
