package creational.builder;

import creational.builder.type.ColorType;
import creational.builder.type.SizeType;

public class CarBuilderImpl implements CarBuilder {
    private SizeType sizeType;
    private ColorType colorType;
    @Override
    public CarBuilder size(SizeType sizeType) {
        this.sizeType = sizeType;
        return this;
    }

    @Override
    public CarBuilder color(ColorType colorType) {
        this.colorType = colorType;
        return this;
    }

    @Override
    public Car build() {
        return new Car(this.sizeType, this.colorType);
    }
}
