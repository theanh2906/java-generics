package creational.builder.type;

public enum SizeType {
    XS, S, M, L, XL
}
