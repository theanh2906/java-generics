package creational.builder.type;

public enum ColorType {
    RED, GREEN, WHITE, BLUE, YELLOW, BLACK, ORANGE, BROWN, PURPLE
}
