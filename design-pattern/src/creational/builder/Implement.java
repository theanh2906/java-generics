package creational.builder;

import creational.builder.type.ColorType;
import creational.builder.type.SizeType;

public class Implement {
    public static void main(String[] args) {
        Car car1 = CarBuilder.newBuilder().size(SizeType.S).build();
        System.out.println(car1.toString());
        Car car2 = CarBuilder.newBuilder().color(ColorType.RED).build();
        System.out.println(car2.toString());
        Car car3 = CarBuilder.newBuilder().size(SizeType.XL).color(ColorType.RED).build();
        System.out.println(car3.toString());
    }
}
