package com.example.demo.rest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.StringRedisTemplate;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/api/redis")
public class RedisController {
    @Autowired
    private StringRedisTemplate template;

    @GetMapping("")
    public void sendMessage() {
        template.convertAndSend("redis-chat", "Hello");
    }
}
