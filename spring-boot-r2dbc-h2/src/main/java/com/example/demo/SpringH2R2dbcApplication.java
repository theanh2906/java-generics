package com.example.demo;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SpringH2R2dbcApplication {

	public static void main(String[] args) {
		SpringApplication.run(SpringH2R2dbcApplication.class, args);
	}

}
