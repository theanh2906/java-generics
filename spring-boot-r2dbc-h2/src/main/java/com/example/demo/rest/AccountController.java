package com.example.demo.rest;

import com.example.demo.models.Accounts;
import com.example.demo.services.AccountService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

@RestController
@RequestMapping("/accounts")
public class AccountController {
    @Autowired
    private AccountService accountService;

    @GetMapping("")
    public Flux<Accounts> getAllAccounts() {
        return accountService.findAll();
    }

    @PostMapping("")
    public Flux<Accounts> addAccount(@RequestBody Accounts account) {
        return accountService.addAccount(account);
    }
}
