package com.example.demo.services;

import com.example.demo.models.Accounts;
import com.example.demo.repositories.AccountRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

@Service
public class AccountService {
    @Autowired
    private AccountRepository accountRepository;

    public Flux<Accounts> findAll() {
        return accountRepository.findAll();
    }

    public Flux<Accounts> addAccount(Accounts account) {
        return accountRepository.save(account).flatMapMany(res -> accountRepository.findAll());
    }
}
