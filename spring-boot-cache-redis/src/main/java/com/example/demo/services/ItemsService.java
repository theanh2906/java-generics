package com.example.demo.services;

import com.example.demo.models.Items;
import com.example.demo.repositories.ItemsRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class ItemsService {
    @Autowired
    private ItemsRepository itemsRepository;

    public List<Items> findAll() {
        return itemsRepository.findAll();
    }

    public Items findById(Long id) {
        return itemsRepository.findById(id).orElse(null);
    }
}
