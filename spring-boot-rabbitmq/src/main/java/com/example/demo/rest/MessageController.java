package com.example.demo.rest;

import com.example.demo.configurations.RabbitMQConfiguration;
import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/api/message")
public class MessageController {
    @Autowired
    private RabbitTemplate rabbitTemplate;
    @GetMapping("")
    public void sendMessage() {
        rabbitTemplate.convertAndSend("spring-boot-exchange", "rabbit.chat", "Hello from RabbitMQ!");
    }
}
