package com.example.demo.rest;

import com.example.demo.models.Items;
import com.example.demo.services.ItemsService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequestMapping("items")
public class ItemsController {
    final Logger LOG = LoggerFactory.getLogger(ItemsController.class);
    @Autowired
    private ItemsService itemsService;
    @GetMapping("")
    public List<Items> findAll() {
        return itemsService.findAll();
    }
    @GetMapping("/{id}")
    public Items findById(@PathVariable Long id) {
        LOG.info("Getting user with ID {}.", id);
        return itemsService.findById(id);
    }
}
