package com.example.demo.rests;

import com.example.demo.services.AuthService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.security.Principal;

@RestController
@RequestMapping("/api/auth")
public class AuthController {
    @Autowired
    private AuthService authService;
    @PreAuthorize("hasAuthority('SCOPE_mod_custom')")
    @GetMapping("")
    public String getMessage(Principal principal) {
        return "Welcome" + principal.getName();
    }

    @PreAuthorize("permitAll()")
    @GetMapping("/token")
    public ResponseEntity<?> getAccessToken() {
        try {
            return ResponseEntity.ok(authService.getAccessToken());
        } catch (Exception e) {
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body(e.getMessage());
        }
    }
}
