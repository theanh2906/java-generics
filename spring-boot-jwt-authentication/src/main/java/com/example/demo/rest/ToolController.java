package com.example.demo.rest;

import com.example.demo.utils.HelpUtils;
import com.google.gson.JsonObject;
import org.json.JSONObject;
import org.springframework.web.bind.annotation.*;

import java.util.Map;

@CrossOrigin(origins = "*", maxAge = 3600)
@RestController
@RequestMapping("/api/tools")
public class ToolController {
    @PostMapping("/encodeJson")
    public String encodeJson(@RequestBody Map json) {
        return HelpUtils.encodeBase64Str(new JSONObject(json).toString());
    }
    @PostMapping("/stringify")
    public String stringify(@RequestBody Map json) {
        return new JSONObject(json).toString();
    }
}
