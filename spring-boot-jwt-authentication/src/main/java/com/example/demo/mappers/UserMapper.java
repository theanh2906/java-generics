package com.example.demo.mappers;

import com.example.demo.dtos.SignupRequest;
import com.example.demo.models.User;
import org.springframework.beans.BeanUtils;

public class UserMapper {
    public static User toModel(SignupRequest dto) {
        final User model = new User();
        BeanUtils.copyProperties(dto, model);
        return model;
    }
}
