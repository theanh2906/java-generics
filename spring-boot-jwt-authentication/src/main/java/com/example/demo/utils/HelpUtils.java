package com.example.demo.utils;

import com.example.demo.dtos.LoginRequest;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;

import java.util.Base64;

public class HelpUtils {
    public static LoginRequest getLoginRequestFromEncodedStr(String encodedStr) {
        ObjectMapper mapper = new ObjectMapper();
        String decodedStr = decodeBase64Str(encodedStr);
        LoginRequest target = new LoginRequest();
        try {
            target = mapper.readValue(decodedStr, LoginRequest.class);
            return target;
        } catch (JsonProcessingException e) {
            e.printStackTrace();
        }
        return target;
    }

    public static String decodeBase64Str(String base64) {
        return new String(Base64.getDecoder().decode(base64));
    }

    public static String encodeBase64Str(String json) {
        return Base64.getEncoder().encodeToString(json.getBytes());
    }

    public static <T> T getObjectFromEncodedStr(String encodedStr, Class<T> clazz) {
        ObjectMapper mapper = new ObjectMapper();
        String decodedStr = decodeBase64Str(encodedStr);
        try {
            return mapper.readValue(decodedStr, clazz);
        } catch (JsonProcessingException e) {
            e.printStackTrace();
        }
        return null;
    }
}
