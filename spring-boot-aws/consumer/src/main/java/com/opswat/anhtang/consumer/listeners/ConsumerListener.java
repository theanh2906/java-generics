package com.opswat.anhtang.consumer.listeners;

import io.awspring.cloud.messaging.listener.annotation.SqsListener;
import org.springframework.stereotype.Component;

@Component
public class ConsumerListener {
    public ConsumerListener() {
        System.err.println("Init " + this.getClass().getName());
    }
    @SqsListener(value = {"theanh2906"})
    public void test() {
        System.err.println("Listen queue 1");
    }
}
