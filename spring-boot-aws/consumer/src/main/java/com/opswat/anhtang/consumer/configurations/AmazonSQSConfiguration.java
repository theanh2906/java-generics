package com.opswat.anhtang.consumer.configurations;

import com.amazonaws.auth.AWSCredentials;
import com.amazonaws.services.s3.AmazonS3;
import com.amazonaws.services.sqs.AmazonSQSAsync;
import com.opswat.anhtang.domain.configurations.AmazonConfiguration;
import io.awspring.cloud.messaging.config.SimpleMessageListenerContainerFactory;
import io.awspring.cloud.messaging.config.annotation.EnableSqs;
import io.awspring.cloud.messaging.core.QueueMessagingTemplate;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Primary;

@Configuration
@EnableSqs
public class AmazonSQSConfiguration {
    @Bean
    @Primary
    public AmazonSQSAsync amazonSQSAsync() {
        return new AmazonConfiguration().amazonSQSAsync();
    }

    @Bean
    public QueueMessagingTemplate queueMessagingTemplate(AmazonSQSAsync amazonSQSAsync) {
        return new QueueMessagingTemplate(amazonSQSAsync);
    }

    @Bean
    @Primary
    public SimpleMessageListenerContainerFactory simpleMessageListenerContainerFactory(AmazonSQSAsync amazonSQSAsync) {
        return new AmazonConfiguration().simpleMessageListenerContainerFactory(amazonSQSAsync);
    }

    @Bean
    public AWSCredentials awsCredentials() {
        return new AmazonConfiguration().awsCredentials();
    }

    @Bean
    public AmazonS3 s3Client() {
        return new AmazonConfiguration().s3Client();
    }
}
