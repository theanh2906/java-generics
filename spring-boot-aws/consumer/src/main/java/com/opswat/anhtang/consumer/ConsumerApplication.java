package com.opswat.anhtang.consumer;

import com.amazonaws.services.s3.AmazonS3;
import io.awspring.cloud.messaging.config.annotation.EnableSqs;
import io.awspring.cloud.messaging.core.QueueMessagingTemplate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.PropertySource;

@SpringBootApplication
@PropertySource("classpath:application.yaml")
public class ConsumerApplication implements CommandLineRunner {
    @Autowired
    private QueueMessagingTemplate messagingTemplate;
    @Autowired
    private AmazonS3 s3Client;
    public static void main(String[] args) {
        SpringApplication.run(ConsumerApplication.class, args);
    }

    @Override
    public void run(String... args) throws Exception {
        messagingTemplate.convertAndSend("theanh2906", "Hello");
        s3Client.listBuckets().forEach(System.out::println);
    }
}
