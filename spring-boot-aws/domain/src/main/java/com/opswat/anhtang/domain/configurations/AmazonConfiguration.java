package com.opswat.anhtang.domain.configurations;

import com.amazonaws.ClientConfiguration;
import com.amazonaws.auth.AWSCredentials;
import com.amazonaws.auth.AWSStaticCredentialsProvider;
import com.amazonaws.auth.BasicAWSCredentials;
import com.amazonaws.client.builder.AwsClientBuilder;
import com.amazonaws.services.s3.AmazonS3;
import com.amazonaws.services.s3.AmazonS3ClientBuilder;
import com.amazonaws.services.sqs.AmazonSQSAsync;
import com.amazonaws.services.sqs.AmazonSQSAsyncClientBuilder;
import com.opswat.anhtang.domain.constants.AWSConfig;
import com.opswat.anhtang.domain.constants.QueueName;
import io.awspring.cloud.messaging.config.SimpleMessageListenerContainerFactory;
import io.awspring.cloud.messaging.core.QueueMessagingTemplate;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.scheduling.concurrent.ThreadPoolTaskExecutor;

public class AmazonConfiguration {
    private final Logger LOG = LoggerFactory.getLogger(getClass());

    public AWSCredentials awsCredentials() {
        return new BasicAWSCredentials(AWSConfig.AWS_CREDENTIAL_ACCESS_KEY, AWSConfig.AWS_CREDENTIAL_SECRET_KEY);
    }

    public AmazonS3 s3Client() {
        return AmazonS3ClientBuilder
                .standard()
                .withCredentials(new AWSStaticCredentialsProvider(awsCredentials()))
                .withRegion(AWSConfig.AWS_S3_REGION).build();
    }

    public AmazonSQSAsync amazonSQSAsync() {
        ClientConfiguration clientConfiguration = new ClientConfiguration();
        clientConfiguration.setMaxConnections(30);
        clientConfiguration.setCacheResponseMetadata(false);
        clientConfiguration.setResponseMetadataCacheSize(0);
        clientConfiguration.setMaxErrorRetry(3);
        AmazonSQSAsync client = builder(clientConfiguration).build();
//        createQueues(client);
        return client;
    }

    public QueueMessagingTemplate queueMessagingTemplate(AmazonSQSAsync amazonSQSAsync) {
        return new QueueMessagingTemplate(amazonSQSAsync);
    }

    public SimpleMessageListenerContainerFactory simpleMessageListenerContainerFactory(AmazonSQSAsync amazonSQSAsync) {
        SimpleMessageListenerContainerFactory factory = new SimpleMessageListenerContainerFactory();
        factory.setAmazonSqs(amazonSQSAsync);
        factory.setWaitTimeOut(10);
        factory.setTaskExecutor(createListenerThreadPool());
        factory.setMaxNumberOfMessages(5);
        return factory;
    }

    private ThreadPoolTaskExecutor createListenerThreadPool() {
        ThreadPoolTaskExecutor executor = new ThreadPoolTaskExecutor();
        executor.setCorePoolSize(5);
        executor.setMaxPoolSize(10);
        executor.setQueueCapacity(0);
        executor.setKeepAliveSeconds(60);
        executor.setWaitForTasksToCompleteOnShutdown(true);
        executor.setAllowCoreThreadTimeOut(true);
        executor.initialize();
        executor.setThreadNamePrefix("webhook-listener-");
        return executor;
    }

    private AmazonSQSAsyncClientBuilder builder(ClientConfiguration clientConfiguration) {
        if (false) {
            LOG.error("Connecting to ElasticMQ ...");
            return AmazonSQSAsyncClientBuilder
                    .standard()
                    .withClientConfiguration(clientConfiguration)
                    .withCredentials(new AWSStaticCredentialsProvider(new BasicAWSCredentials("x", "x")))
                    .withEndpointConfiguration(new AwsClientBuilder.EndpointConfiguration(AWSConfig.SQS_ENDPOINT, "elasticmq"));
        } else {
            LOG.error("Connecting to AWS SQS ...");
            return AmazonSQSAsyncClientBuilder
                    .standard()
                    .withClientConfiguration(clientConfiguration)
                    .withEndpointConfiguration(new AwsClientBuilder.EndpointConfiguration(AWSConfig.AWS_SQS_API, AWSConfig.AWS_SQS_REGION))
                    .withCredentials(new AWSStaticCredentialsProvider(awsCredentials()));
        }
    }

    private void createQueues(AmazonSQSAsync client) {
        QueueName.getAllQueues().forEach(client::createQueue);
    }
}
