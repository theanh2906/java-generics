package com.opswat.anhtang.domain.constants;

import java.util.Arrays;
import java.util.List;

public class QueueName {
    public static final String QUEUE_1 = "queue_1";
    public static final String QUEUE_2 = "queue_2";
    public static final String QUEUE_3 = "queue_3";
    public static final String QUEUE_4 = "queue_4";

    public static List<String> getAllQueues() {
        return Arrays.asList(QUEUE_1, QUEUE_2, QUEUE_3, QUEUE_4);
    }
}
