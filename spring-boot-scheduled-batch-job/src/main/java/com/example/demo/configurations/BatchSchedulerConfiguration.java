package com.example.demo.configurations;

import com.example.demo.models.Book;
import com.example.demo.repositories.BookRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.batch.core.Job;
import org.springframework.batch.core.JobExecution;
import org.springframework.batch.core.JobParametersBuilder;
import org.springframework.batch.core.Step;
import org.springframework.batch.core.configuration.annotation.EnableBatchProcessing;
import org.springframework.batch.core.configuration.annotation.JobBuilderFactory;
import org.springframework.batch.core.configuration.annotation.StepBuilderFactory;
import org.springframework.batch.core.launch.JobLauncher;
import org.springframework.batch.core.launch.support.RunIdIncrementer;
import org.springframework.batch.item.ItemWriter;
import org.springframework.batch.item.file.FlatFileItemReader;
import org.springframework.batch.item.file.builder.FlatFileItemReaderBuilder;
import org.springframework.batch.item.file.mapping.BeanWrapperFieldSetMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.io.ClassPathResource;
import org.springframework.scheduling.TaskScheduler;
import org.springframework.scheduling.annotation.EnableScheduling;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.scheduling.concurrent.ThreadPoolTaskScheduler;
import org.springframework.scheduling.support.ScheduledMethodRunnable;

import java.util.Date;
import java.util.IdentityHashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ScheduledFuture;
import java.util.concurrent.atomic.AtomicBoolean;
import java.util.concurrent.atomic.AtomicInteger;

@Configuration
@EnableBatchProcessing
@EnableScheduling
public class BatchSchedulerConfiguration {
    private final Logger LOG = LoggerFactory.getLogger(BatchSchedulerConfiguration.class);
    private AtomicBoolean enabled = new AtomicBoolean(true);
    private AtomicInteger batchRunCounter = new AtomicInteger(0);
    private final Map<Object, ScheduledFuture<?>> scheduleTasks = new IdentityHashMap<>();
    @Autowired
    private JobBuilderFactory jobBuilderFactory;
    @Autowired
    private StepBuilderFactory stepBuilderFactory;
    @Autowired
    private JobLauncher jobLauncher;
    @Autowired
    private BookRepository bookRepository;
    @Scheduled(fixedRate = 10000)
    public void launchJob() throws Exception {
        Date date = new Date();
        LOG.debug("Scheduler starts at {}", date);
        if (enabled.get()) {
            JobExecution jobExecution = jobLauncher.run(importBooks(), new JobParametersBuilder().addDate("launchDate", date).toJobParameters());
            batchRunCounter.incrementAndGet();
            LOG.debug("Batch job ends with status as {}", jobExecution.getStatus());
        }
        LOG.debug("Scheduler ends");
    }

    public void stop() {
        enabled.set(false);
    }

    public void start() {
        enabled.set(true);
    }

    @Bean
    public TaskScheduler poolScheduler() {
        return new CustomTaskScheduler();
    }

    private class CustomTaskScheduler extends ThreadPoolTaskScheduler {
        private static final long serialVersionUID = -7142624085505040603L;

        @Override
        public ScheduledFuture<?> scheduleAtFixedRate(Runnable task, long period) {
            ScheduledFuture<?> scheduledFuture = super.scheduleAtFixedRate(task, period);
            ScheduledMethodRunnable runnable = (ScheduledMethodRunnable) task;
            scheduleTasks.put(runnable.getTarget(), scheduledFuture);
            return scheduledFuture;
        }
    }

    public void cancelFutureScheduledTasks() {
        scheduleTasks.forEach((key, value) -> {
            if (key instanceof BatchSchedulerConfiguration) {
                value.cancel(false);
            }
        });
    }

    @Bean
    public Job importBooks() {
        return jobBuilderFactory.get("importBooks")
                .incrementer(new RunIdIncrementer())
                .flow(importBookStep())
                .end()
                .build();
    }

    @Bean
    public Step importBookStep() {
        return stepBuilderFactory.get("importBookStep")
                .<Book, Book>chunk(2)
                .reader(reader())
                .writer(writer())
                .build();
    }

    @Bean
    public FlatFileItemReader<Book> reader() {
        return new FlatFileItemReaderBuilder<Book>()
                .name("bookItemReader")
                .resource(new ClassPathResource("book.csv"))
                .delimited()
                .names("name")
                .fieldSetMapper(new BeanWrapperFieldSetMapper<Book>() {
                    {
                        setTargetType(Book.class);
                    }
                })
                .build();
    }

    @Bean
    public ItemWriter<Book> writer() {
        return new ItemWriter<Book>() {
            @Override
            public void write(List<? extends Book> items) throws Exception {
                LOG.debug("Writer...{}", items.size());
                for (Book item: items) {
                    bookRepository.save(item);
                    LOG.debug("Imported book with id {}", item.getId());
                }
            }
        };
    }

    public AtomicInteger getBatchRunCounter() {
        return batchRunCounter;
    }
}
